#ifndef __LIBGFX_DRAW_H
#define __LIBGFX_DRAW_H

#include <stdint.h>

void draw_square(uint32_t x, uint32_t y, uint32_t width, uint32_t height, uint32_t color);

#endif
